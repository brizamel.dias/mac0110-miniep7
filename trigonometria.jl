# MAC0110-MiniEP7
# Briza Mel Dias de Sousa - 11796357

function sin(x)
    sinx = x
    n = 3
    for cont in 1:10
        sinx = sinx + (((-1)^cont) * x^n)/ factorial(big(n)) 
        n = n + 2
    end
    return sinx
end

function cos(x)
    cosx = 1
    n = 2
    for cont in 1:10 
        cosx = cosx + (((-1)^cont) * x^n)/ factorial(n)
        n = n + 2
    end
    return cosx
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m+1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j*(A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end



function tan(x) 
    tanx = x
    n = 2
    for cont in 1:10
        tanx = tanx + ((2^(2n)*(2^(2n) - 1))*bernoulli(n)*x^(2n-1))/factorial(big
        (2n))
        n = n + 1
    end
    return tanx
end

function check_sin(value, x)
    erro = 0.0001
    if abs(value - sin(x)) < erro 
        return true
    else
        return false
    end
end

function check_cos(value, x)
    erro = 0.0001
    if abs(value - cos(x)) < erro 
        return true
    else
        return false
    end
end

function check_tan(value, x)
    erro = 0.0001
    if abs(value - tan(x)) < erro 
        return true
    else
        return false
    end
end

<<<<<<< HEAD
function taylor_sin(x)
    sinx = x
    n = 3
    for cont in 1:10
        sinx = sinx + (((-1)^cont) * x^n)/ factorial(big(n)) 
        n = n + 2
    end
    return sinx
end

function taylor_cos(x)
    cosx = 1
    n = 2
    for cont in 1:10 
        cosx = cosx + (((-1)^cont) * x^n)/ factorial(n)
        n = n + 2
    end
    return cosx
end

function taylor_tan(x) 
    tanx = x
    n = 2
    for cont in 1:10
        tanx = tanx + ((2^(2n)*(2^(2n) - 1))*bernoulli(n)*x^(2n-1))/factorial(big
        (2n))
        n = n + 1
    end
    return tanx
end

using Test
    erro = 1e-6
    begin
        @test abs(sin(pi/2) -1) < erro
        @test abs(sin(pi/3) - sqrt(3)/2) < erro
        @test abs(cos(pi/4) - sqrt(2)/2) < erro
        @test abs(cos(0) - 1) < erro
        @test abs(tan(pi/4) - 1) < erro
        @test abs(tan(0)) < erro
        @test check_sin(0.5, pi/6) == true
        @test check_sin(sqrt(3)/2, pi/3) == true
        @test check_cos(sqrt(3)/2, pi/6) == true
        @test check_cos(0.5, pi/3) == true
        @test check_tan(1, pi/4) == true
        @test check_tan(0, 0) == true
    end
    println("Final dos testes.")
end

test()
>>>>>>> parte3
